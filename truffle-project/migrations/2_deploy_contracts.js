const HelloWorld = artifacts.require('./HelloWorld.sol');
const Unprotected = artifacts.require("./Unprotected.sol");
const Delegate = artifacts.require("./Delegate.sol");
const Delegation = artifacts.require("./Delegation.sol");
const Ownable = artifacts.require("./open-zeppelin/Ownable.sol");
const DoS = artifacts.require("./DoS.sol");
const DoSAttack = artifacts.require("./DoSAttack.sol");
const Underflow = artifacts.require('./Underflow.sol');
const Reentrancy = artifacts.require('./Reentrancy.sol');
const BadRandom = artifacts.require('./BadRandom.sol');

const BAD_RANDOM_SEED = 314;
const UNDERFLOW_SUPPLY = 1000;

module.exports = deployer => {
  deployer.deploy(HelloWorld);
  deployer.deploy(Unprotected);
  deployer.deploy(Ownable);
  deployer.deploy(DoS, {from: web3.eth.accounts[8],value: web3.toWei(0.01, 'ether')});
  deployer.deploy(DoSAttack, {from: web3.eth.accounts[9], value: web3.toWei(0.01, 'ether')});
  deployer.deploy(Delegate, web3.eth.accounts[0]).then(()=> {
	return deployer.deploy(Delegation, Delegate.address)
	})
  deployer.deploy(BadRandom, BAD_RANDOM_SEED, {
    value: web3.toWei(5, 'ether')
  });
  deployer.deploy(Underflow, UNDERFLOW_SUPPLY);
  deployer.deploy(Reentrancy, {
    value: web3.toWei(5, 'ether')
  });
}
