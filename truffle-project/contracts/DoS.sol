pragma solidity ^0.4.24;

import "./open-zeppelin/Ownable.sol";

contract DoS is Ownable {
	address public king;
	uint public prize;
	
	function DoS() public payable {
		king = msg.sender;
		prize = msg.value;
	}

	function() external payable{
		require(msg.value >= prize || msg.sender == owner);
		king.transfer(msg.value);
		king = msg.sender;
		prize = msg.value;		
	}

}
