pragma solidity ^0.4.24;

contract Unprotected {
    address public owner;

    modifier restricted() {
        require(msg.sender == owner);
        _;
     }

    function Unprotected() public {
        owner = msg.sender;
    }

    function changeOwner(address _newOwner)
	public
    {
	owner = _newOwner;
    }

   function changeOwner_fixed(address _newOwner)
	public
	restricted
    {
    	owner = _newOwner; 
    }
}
