pragma solidity ^0.4.24;


contract BadRandom {
  uint256 private seed;
  uint8 public lastResult;
  bool public winner = false;

  constructor(uint _seed) public payable {
    seed = _seed;
  }

  /**
    * @dev Generates a pseudo-random number between 0 and 46
    */
  function getRandomNumber(uint256 _seed) internal view returns (uint8) {
    return uint8(uint256(keccak256(block.blockhash(block.number), block.timestamp, _seed)) % 47);
  }

  /**
    * @dev Throws a random number and compares it to a number chosen by the caller.
    * If the numbers match, the whole jackpot is transferred to the caller.
    * @return True if the caller won. False otherwise.
    */
  function bet(uint8 _bet) public payable returns (bool) {
    require(winner == false && msg.value > 0 && _bet >= 0 && _bet <= 46);

    lastResult = getRandomNumber(seed);

    if(lastResult == _bet) {
      msg.sender.transfer(address(this).balance);
      winner = true;
    }
    return winner;
  }
}
