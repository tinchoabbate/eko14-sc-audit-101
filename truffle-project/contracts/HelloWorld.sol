pragma solidity ^0.4.24;


contract HelloWorld {
  address public owner;
  string public message = "Hello Ekoparty world!";

  // DEPRECATED
  // function HelloWorld() public { ... }

  constructor() public {
    owner = msg.sender;
  }

  function setMessage(string _message) public {
    message = _message;
  }

  function () external payable {}
}
