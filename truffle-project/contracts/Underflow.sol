pragma solidity ^0.4.24;

contract Underflow {
  mapping (address => uint256) tickets;
  uint256 public ticketTotalSupply;

  constructor (uint256 _ticketTotalSupply) public {
    ticketTotalSupply = _ticketTotalSupply;
    tickets[msg.sender] = _ticketTotalSupply;
  }

  function ticketsOf(address _addr) public view returns (uint256) {
    return tickets[_addr];
  }

  function transfer(address _to, uint256 _amount) public {
    // Check that sender has enough tickets to transfer
    uint256 ticketsLeft = tickets[msg.sender] - _amount;
    require(ticketsLeft >= 0);

    // Update balances
    tickets[msg.sender] -= _amount;
    tickets[_to] += _amount;
  }

  /* Logic to buy tickets and so on... */
}
