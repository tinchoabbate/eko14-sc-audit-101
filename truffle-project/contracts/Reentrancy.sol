pragma solidity ^0.4.24;


contract Reentrancy {
  mapping(address => uint256) private deposits;

  constructor() public payable {
    deposits[msg.sender] = msg.value;
  }

  function depositsOf(address _address) public view returns (uint256) {
    return deposits[_address];
  }

  /**
   * @dev allows caller to withdraw funds
   */
  function withdraw() public {
    uint256 amount = deposits[msg.sender];

    // Send funds
    msg.sender.call.value(amount)();

    // Update balance
    deposits[msg.sender] -= amount;
  }

  /**
   * @dev payable fallback to make a deposit
   */
  function () external payable {
    deposits[msg.sender] += msg.value;
  }
}
