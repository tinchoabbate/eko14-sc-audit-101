FROM node:8
WORKDIR usr/src/workshop
RUN npm i -g truffle ganache-cli
COPY ./truffle-project .
ENTRYPOINT /bin/bash
