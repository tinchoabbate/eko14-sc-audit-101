root@kali:~/Documents/workshop# ./mythril.sh Reentrancy.sol

...

==== Message call to external contract ====
Type: Warning
Contract: Reentrancy
Function name: withdraw()
PC address: 371
This contract executes a message call to the address of the transaction sender. Generally, it is not recommended to call user-supplied addresses using Solidity's call() construct. Note that attackers might leverage reentrancy attacks to exploit race conditions or manipulate this contract's state.
--------------------
In file: /contracts/Reentrancy.sol:22

msg.sender.call.value(amount)()

--------------------

==== State change after external call ====
Type: Warning
Contract: Reentrancy
Function name: withdraw()
PC address: 451
The contract account state is changed after an external call. Consider that the called contract could re-enter the function before this state change takes place. This can lead to business logic vulnerabilities.
--------------------
In file: /contracts/Reentrancy.sol:25

deposits[msg.sender] -= amount

--------------------

