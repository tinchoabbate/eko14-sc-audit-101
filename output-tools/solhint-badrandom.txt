
...

  17:67  warning  Avoid to make time-based decisions in your business logic                        not-rely-on-time
  17:36  warning  Do not rely on "block.blockhash". Miners can influence its value                 not-rely-on-block-hash

...

✖ 24 problems (20 errors, 4 warnings)

