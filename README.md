# Smart Contracts Audit 101 - Workshop Ekoparty 2018

## Requirements

- [Docker](https://docs.docker.com/install/)

## Testing environment
1. Clone this repository
2. Run `docker build -t badbounty/workshop .`
3. Once the image is built, run `docker run -it badbounty/workshop`
4. In the opened shell, run `ganache-cli` to start the ganache service inside the container

### Usage
#### Open a new shell inside the container
- Copy the container ID and run `docker exec -it <CONTAINER-ID> bash`

#### Deploy contracts
- Open a new container shell and run `truffle migrate`

#### Execute an exploit
- Open a new container shell and run `truffle exec exploits/<exploit-name>.js`. *Bear in mind that you must have previously deployed the corresponding vulnerable contract for exploit to work.*

## Static analysis tools

> In both cases, the Solidity files are expected to be inside the `./truffle-project/contracts` folder

### Oyente
- Install: `docker pull luongnguyen/oyente`
- Usage: `./oyente.sh <contract-name>.sol`

### Mythril
- Install: `docker pull mythril/myth`
- Usage: `./mythril.sh <contract-name>.sol`
